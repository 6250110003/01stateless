import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PSU Trang",
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.home),
          title: Text('Student App'),
          actions: [IconButton(onPressed: (){}, icon: Icon(Icons.settings))],
        ),
        body: Center(
          child: Text('Napatsorn Bukong',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 35
          ),),

        ),
      ),
    );
  }
}

